﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {
    int hp = 5;
    int score = 0;
    float speed = 1;
	int amm = 10;
	public GameObject bullet;
    public GameObject hpboard;
    public GameObject scoreboard;
	public GameObject ammo;
	bool Untouch = false;
	float untouchTimer = 0;
	Rigidbody rb;
	// Use this for initialization

	void Start () {
		rb = GetComponent<Rigidbody> ();
		showUI ();
	}
	
	// Update is called once per frame
	void Update () {
		rb.velocity= new Vector3((float)Input.GetAxis("Horizontal")*10f*speed , 0f,  (float)Input.GetAxis("Vertical")*10f*speed);
		if (Input.GetKeyDown(KeyCode.Space))
			Fire();
		Tick ();
    }

	void showUI()
	{
		hpboard.GetComponent<UnityEngine.UI.Text>().text = "HP  " + hp;
		scoreboard.GetComponent<UnityEngine.UI.Text>().text = "SCORE  " + score;
		ammo.GetComponent<UnityEngine.UI.Text>().text = "AMMO  " + amm;
	}

	void Fire()
	{
		if (amm > 0) {
			GameObject r = (GameObject)	Instantiate (bullet, new Vector3( transform.position.x, transform.position.y, transform.position.z + 1.9f) , transform.rotation );	
			amm--;
		}
	}

    void OnTriggerEnter(Collider c)
    {
       if(c.gameObject.tag == "Gold")
       {
           score++;
		   showUI ();
           Destroy(c.gameObject);

       }
    }

	public void AddHp(int i)
	{
		if (i > 0 || !Untouch)
		hp += i;
		if (hp <= 0) UnityEngine.SceneManagement.SceneManager.LoadScene(0);
		showUI ();
	}

	public int GetScore()
	{
		return score;
	}

	public void SetSpeed(float s)
	{
		speed += s;
	}
	public void setAmmo(int a)
	{
		amm += a;
		showUI ();
	}
	public void SetUntouch(bool u, float t)
	{
		Untouch = u;
		untouchTimer = t;
	}

	void Tick()
	{
		if(untouchTimer <= 0)
		    untouchTimer -= 0.03f;
		else
			Untouch = false;	
	}
}
