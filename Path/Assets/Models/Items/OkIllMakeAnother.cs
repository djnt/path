﻿using UnityEngine;
using System.Collections;

public class OkIllMakeAnother : MonoBehaviour {

	public int hpChange;
	public float speedChange;
	public int ammoChange;
	public bool Untouch;
	public Vector2 spawnAreaX = Vector2.zero;
	public Vector2 spawnAreaY = Vector2.zero;


	float timer;
	//bool wait = false;

	// Use this for initialization

	void Start () {
		timer = Random.Range (10f, 20f);
	}

	// Update is called once per frame
	void Update () {
		Tick ();
		if (Input.GetKeyDown (KeyCode.Q))
			Spawn ();
	}



	void Tick()
	{
		
		//if (!wait) {
			timer -= 0.1f;
		//}
		if (timer <= 0 /*&& !wait*/) {
			//wait = true;
			timer = Random.Range(20f, 25f);
			Spawn ();
		}

	}

	void Spawn()
	{
		transform.position = new Vector3 (Random.Range (spawnAreaX.x, spawnAreaX.y), -3.15f, Random.Range (spawnAreaY.x, spawnAreaY.y));

	}

	void OnTriggerEnter(Collider c)
	{
		/*if (c.gameObject.tag != "Player" && c.gameObject.tag != "Enemy")
			Spawn ();
			*/ 
		if (c.gameObject.tag == "Player") {
			transform.position = new Vector3 (0, 50f, 0);
			c.gameObject.GetComponent<PlayerController> ().SetSpeed (speedChange);
			c.gameObject.GetComponent<PlayerController> ().AddHp (hpChange);
			c.gameObject.GetComponent<PlayerController> ().setAmmo (ammoChange);
			c.gameObject.GetComponent<PlayerController> ().SetUntouch (true, 10f);
//			wait = false;
			timer = Random.Range(1f, 6f);
		}

	}
}
