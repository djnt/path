using UnityEngine;
using System.Collections;

public class BonusCOntroller : MonoBehaviour {
	public int hpChange;
	public float speedChange;
	public int ammoChange;
	public Vector3 spawnArea;
	float timer = Random.Range(1f, 6f);
	bool wait = false;

	// Use this for initialization

	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Tick ();
	}



	void Tick()
	{
		if (!wait) {
			timer -= 0.1f;
		}
		if (timer <= 0) {
			wait = true;
			Spawn ();
		}
			
	}

	void Spawn()
	{
		transform.position = new Vector3 (Random.Range (spawnArea.x, spawnArea.y), 0, Random.Range (spawnArea.x, spawnArea.z));

	}

	void OnTriggerEnter(Collider c)
	{
		if (c.gameObject.tag != "Player" && c.gameObject.tag != "Enemy")
			Spawn ();
		if (c.gameObject.tag != "Player") {
			transform.position = new Vector3 (0, 50f, 0);
			c.gameObject.GetComponent<PlayerController> ().SetSpeed (speedChange);
			c.gameObject.GetComponent<PlayerController> ().AddHp (hpChange);
			c.gameObject.GetComponent<PlayerController> ().setAmmo (ammoChange);
			wait = false;
			timer = Random.Range(1f, 6f);
		}
			
	}
}
