﻿using UnityEngine;
using System.Collections;

public class Search : MonoBehaviour {
   public GameObject target;
    public GameObject spot;
    GameObject curr;
    public ParticleSystem ps;
    NavMeshAgent nm;

	void Start () {
        nm = GetComponent<NavMeshAgent>();
        curr = spot;
	}
	
	// Update is called once per frame
	void Update () {
       nm.SetDestination(curr.transform.position);
	}

    void OnCollisionEnter(Collision c)
    {
		
        if (c.gameObject.tag == "Player")
        {
			c.gameObject.GetComponent<PlayerController>().AddHp(-1);
			c.gameObject.GetComponent<Renderer>().material.color = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
			Boom ();
        }
    }

	public void SetCurr(GameObject cr)
	{
		curr = cr;
	}

	public void Boom()
	{
		Instantiate(ps,new Vector3( transform.position.x, transform.position.y + 5, transform.position.z), Quaternion.identity);
		Destroy(transform.gameObject);
	}
}
