﻿using UnityEngine;
using System.Collections;

public class TargDetector : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnCollisionEnter(Collision c)
    {
        if (c.gameObject.tag == "Player")
        {
        foreach (GameObject go in GameObject.FindGameObjectsWithTag("Enemy"))
            {
				go.GetComponent<Search>().SetCurr(go.GetComponent<Search>().target);
            }
        }
    }
    void OnCollisionExit(Collision c)
    {
        if (c.gameObject.tag == "Player")
        {
            foreach (GameObject go in GameObject.FindGameObjectsWithTag("Enemy"))
            {
				go.GetComponent<Search>().SetCurr( go.GetComponent<Search>().spot);
            }
			if (c.gameObject.GetComponent<PlayerController>().GetScore() == 6) UnityEngine.SceneManagement.SceneManager.LoadScene(1);
        }

    }

    
}
